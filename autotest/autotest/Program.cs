﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium;

namespace autotest
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Url = "https://www.kinopoisk.ru/";
            IWebElement searchField = driver.FindElement(By.XPath("//form/input[@name='kp_query']"));
            searchField.SendKeys("Список Шиндлера");
            IWebElement searchButton = driver.FindElement(By.XPath("//span/input[@value='искать!']"));
            searchButton.Click();
            driver.Close();
        }
    }
}
